
//!
//! 
//! ```
//!     use const_std_vec::ConstVec;
//!     const TEST : &'static ConstVec<u8> = &ConstVec::new(b"123");
//!     assert_eq!(b"123",TEST.as_vec().as_slice());
//! ```
//! 

pub struct ConstVec<T:'static> {
    _buf: &'static [T],
    _len: usize,
}

impl<T:'static> ConstVec<T>{
    pub const fn new(static_ref:&'static [T])->ConstVec<T>{
        ConstVec{
            _buf: static_ref,
            _len: static_ref.len(),
        }
    }
    #[inline]
    pub const fn as_vec(&'static self)->&'static Vec<T>{
        unsafe {
            core::mem::transmute(self)
        }
    }
}