# const_std_vec



```rust
    use const_std_vec::ConstVec;
    const TEST : &'static ConstVec<u8> = &ConstVec::new(b"123");
    assert_eq!(b"123",TEST.as_vec().as_slice());
```


License: MIT OR Apache-2.0
